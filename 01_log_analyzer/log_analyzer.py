import argparse
import json
import os
import logging
import re
from datetime import datetime
from gzip import open as gopen
from collections import OrderedDict
from statistics import median


def get_last_log(path):
    last_log = None
    last_log_date = 0
    for f in os.listdir(path):
        match = re.match(r'^nginx-access-ui\.log-(?P<date>\d{8})(\.gz)?$', f)
        if match and int(match.groupdict()['date']) > last_log_date:
            last_log = match[0]
            last_log_date = int(match.groupdict()['date'])
    return last_log, str(last_log_date)


def report_exist(path, rdate):
    report_name = 'report-'+datetime.strptime(rdate, '%Y%m%d').strftime('%Y.%m.%d.')+'html'
    return report_name in os.listdir(path)


def parse_line(input_str):
    input_str = input_str.decode('utf-8')
    url = re.findall(r'\"([GET|POST|HEAD|PUT|OPTIONS].+) HTTP\/1', input_str)
    time = float(input_str.split()[-1])
    return url[0] if len(url) > 0 else 'unrecognized', time


def line_generator(logfile):
    open_method = gopen if file.split('.')[-1] == 'gz' else open
    try:
        with open_method(logfile, 'rb') as f:
            for line in f:
                yield line
    except (FileExistsError, FileNotFoundError):
        logging.error(f'Cant read {logfile}')
        raise


def parse_file(f, limit):
    req_timing = {}
    total = 0
    parsed = 0
    summary_time = 0
    # getting timings from file
    try:
        for line in line_generator(f):
            total += 1
            url, time = parse_line(line)
            if url not in req_timing.keys():
                req_timing[url] = []
            req_timing[url].append(time)
            if url != 'unrecognized':
                parsed += 1
                summary_time += time
    except (FileNotFoundError, FileExistsError):
        raise
    if (parsed/total)*100 < 97:
        logging.warning("Parsing persentage lower 97")
    logging.info(f'{f} pared {(parsed / total) * 100} perscent...')
    # generating report content
    content = {}
    for req in req_timing.keys():
        current_req_times = req_timing[req]
        new_record = {
            "count": len(current_req_times),
            "time_max": max(current_req_times),
            "time_sum": sum(current_req_times),
            "time_perc": (sum(current_req_times) / summary_time) * 100,
            "count_perc": (len(current_req_times) / total) * 100,
            "time_avg": (sum(current_req_times) / len(current_req_times)) / 1000,
            "time_med": median(current_req_times)
        }
        content[req] = new_record
    # cuting results to limit
    result = dict(OrderedDict(sorted(content.items(), key=lambda x: x[1]['time_sum'], reverse=True)[0:limit]))
    logging.info(f'{len(content)} trimmed to {limit}...')
    return result


def gen_report(patch, log_date, content):
    logging.info(f'writing report file...')
    # reading template
    try:
        with open('report.html', 'r') as f:
            template_content = f.read()
    except (FileNotFoundError, FileExistsError):
        logging.error('Cant open template!!')
        raise
    template_content = template_content.replace('$table_json', json.dumps(content))
    # writing report to file
    filename = 'report-' + datetime.strptime(log_date, '%Y%m%d').strftime('%Y.%m.%d.') + 'html'
    filename = os.path.join(patch, filename)
    try:
        with open(filename, 'w') as f:
            f.write(template_content)
        logging.info(f'report in {filename}')
    except(FileNotFoundError, FileExistsError):
        logging.error('cant write report file')
        raise


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", dest='conf', default='./config.json')
    args = parser.parse_args()
    with open(args.conf, 'r') as f:
        config = json.loads(f.read())
    logging.basicConfig(filename=config.get('OUT_FILE') if config.get('OUT_FILE') != "None" else None,
                        level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s')
    logging.info('We started')
    try:
        file, date = get_last_log(config.get('LOG_DIR'))
        if not report_exist(config.get('REPORT_DIR'), date):
            logging.info(f' parsing {os.path.join(config.get("LOG_DIR"), file)}...')
            file_content = parse_file(os.path.join(config.get("LOG_DIR"), file), config.get("REPORT_SIZE"))
            gen_report(config.get('REPORT_DIR'), date, file_content)
            logging.info(f'Done')
        else:
            logging.info('report already exit, exitting')
    except (FileExistsError, FileNotFoundError):
        logging.error('folders in config is incorrect')
